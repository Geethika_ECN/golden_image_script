INTRODUCTION
This is a bash shell script to create golden image for linux operating system.(flavor=rhel)
--------------------------------------------------------------------------------------------------------------
1. clone this repo into hci node in /root/enclouden/utils
2. git clone the repository 
3. Go inside repository
4. Execute the shell script: sh linux_make.sh
---------------------------------------------------------------------------------------------------------------
Now you can use the golden image from sysadmin dashboard to create VMs.
