#!/bin/bash

make() {
echo "======================== Provide Basic Details: ============================ "
echo
name=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 5 ; echo '')
read -p "OS Family (rhel/debian): " ostype
read -p "OS Variant (Check make.txt file for OS Variants): " osvar
value=1024
ram=$((4 * value))
echo "RAM (GB):" 4
vcpus=4
echo "VCPUs:" 4
dp=15
echo "Disk Size (GB):" 15
read -p "ISO (Default:/root/enclouden/images): " iso
echo "======================= Starting NAT network =============================== "
virsh net-start default > /dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "Started network Default:NAT"
else
        echo "Network already active"
fi
echo
echo "====================== Creating virt-vm, please carry out the installation manually ========================= "
virt-install   -n $name   --description "Golden Image VM"   --os-type=Linux   --os-variant=$osvar   --ram=$ram   --vcpus=$vcpus   --disk path=/var/lib/libvirt/images/$name.qcow2,bus=scsi,size=$dp   --graphics vnc   --cdrom /root/enclouden/images/$iso   --network default
echo
read -p "Username of the user created  during installation: " user
x=$(virsh domifaddr $name | awk 'NR==3 {print $4}' | cut -d "/" -f1)
echo
echo "Please enter password when prompted"
sleep 5
if [ $ostype == 'rhel' ]
then
        ssh $user@$x "yum update -y  && sed -i '38s/^#//g' /etc/ssh/sshd_config  && sed -i '38s/no/yes/g' /etc/ssh/sshd_config && sed -i '65s/no/yes/g' /etc/ssh/sshd_config && systemctl restart sshd && init 6"
else
		    ssh $user@$x "apt update -y  && sed -i '38s/no/yes/g' /etc/ssh/sshd_config && sed -i '65s/no/yes/g' /etc/ssh/sshd_config && service sshd restart  && init 6"
fi
sleep 60
echo "Please enter password when prompted"
sleep 5
if [ $ostype == 'rhel' ]
then
        ssh $user@$x "yum install -y spice-vdagent &&  yum install -y cloud-init && init 0"
else
        ssh $user@$x "apt install -y spice-vdagent &&  apt install -y cloud-init && init 0"
fi
echo
echo "====================== Finished making golden image ========================== "
echo

echo "====================== Removing qcow2 from nova hypervisor =================== "
cp /var/lib/libvirt/images/$name.* /root/enclouden/images
cd /root/enclouden/images

echo "Converting to raw"; qemu-img convert $name.qcow2 $name.raw; echo "Image is at /root/enclouden/images/$name.raw";
echo
echo " ======================== Deleting vm from hypervisor =========================== "
rm -rf /var/lib/libvirt/images/$name.*
}

make

echo "############### UPLOADING ENCLOUDEN GOLDEN IMAGE ##############"

# ---------------------------------------------------------------------------+

# -------path of raw image file ------------------                           |
path=("/root/enclouden/images/$name.raw")
# -------name which need to set for image --------                           |
name=("$name")
# -------image format ----------------------------                           |
format=("raw")
# -------os type - linux or windows --------------                           |
os_type=("linux")
# -------windows or centos or ubuntu or cirros or rhel -------------         |
os_distro=("centos")                      |                                                                       |
# ---------------------------------------------------------------------------+

# ***************************************************************#################################******************************************************************************
# ***************************************************************#################################*****************************************************************************
echo
echo "############### VARIABLES EXPORTED ############################"
echo
source /root/adminrc

echo "############### CREATING NEW IMAGE ############################"
echo

glance image-create --disk-format $format --container-format bare --visibility public --file $path --name $name --progress
echo "############### SUCCESSFULLY UPLOADED NEW IMAGE ################"
echo
echo "############### SETTING PROPERTIES TO IMAGE ####################"

openstack image set --property hw_qemu_guest_agent=yes --property hw_video_model=qxl --property hw_video_ram=128 --property hw_rng_model=virtio --property hw_scsi_model=virtio-scsi --property hw_disk_bus=scsi --property os_require_quiesce=yes --property os_type=$os_type --property os_distro=$os_distro --property hw_cdrom_bus=sata $name

echo
echo "############### SUCCESSFULLY SET IMAGE PROPERTIES ##############"
echo
echo "############### LIST OF IMAGES #################################"
echo

openstack image list
echo
echo "############### INFO OF NEW IMAGE ##############################"

openstack image show $name

echo
echo "######################## ********** CHEERS!!! HAVE A GOOD DAY ************* ##################################"